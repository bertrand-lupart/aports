# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>

pkgname=font-iosevka
pkgver=11.2.0
pkgrel=0
pkgdesc="Versatile typeface for code, from code."
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab
	$pkgname-aile
	$pkgname-etoile
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-aile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-etoile-$pkgver.zip
	"

builddir="$srcdir"

_install_font_ttc() {
	font="$srcdir/$1.ttc"
	install -Dm644 "$font" -t "$subpkgdir"/usr/share/fonts/TTC
}

package() {
	for pkg in $subpackages; do
		depends="$depends $pkg"
	done
	mkdir -p "$pkgdir"
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	_install_font_ttc "iosevka"
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	_install_font_ttc "iosevka-slab"
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	_install_font_ttc "iosevka-curly"
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	_install_font_ttc "iosevka-curly-slab"
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	_install_font_ttc "iosevka-aile"
}

etoile() {
	pkgdesc="$pkgdesc (Iosevka Etoile)"
	_install_font_ttc "iosevka-etoile"
}

sha512sums="
4340e9b78ac079965de2bb7699565cb8066f24ee48a482100936e71c6eb2aa93160ceb3cbbb38ed3b6aa259b7fed85f5487bbdf1cd76332bc47f70f0ec0328e0  super-ttc-iosevka-11.2.0.zip
3cc617538f3e3bdbf6b631d925a53620ef0a27f909b3a5a8d0bd31239752311b11b73fe8a03b0662dc42c178c6a39173d3b7ab598f0d2340faedacf7b9b382f1  super-ttc-iosevka-slab-11.2.0.zip
d11ffdf57cc8e5af4399e9424d75e5eec2ad7323eec842c2c1aa41cd8ccac2eb54f93d214cca2662b47037e53065a2e756ba9dac71e17b2140813a57cd52b17d  super-ttc-iosevka-curly-11.2.0.zip
f1ddff088e3396e5aae21b36c8b8e906ec78ce973324dfe38bc1a2500fbd850588f72848f28bc0d0498f34169faaf3595a3fecfbd0856dd906765e06a148ad23  super-ttc-iosevka-curly-slab-11.2.0.zip
494d01a432095f3b2600e2bc7be8b21ecb5b5102879693abd7a0accc5787404c81e8acbd2bf47cefc6ffa9e31ac5cea5c1f6cc8f0f73a6f2d5a46bf6ac22ddd7  super-ttc-iosevka-aile-11.2.0.zip
27c7c605c48001eaa1f01a50915bf9444f5bbd091e6f7a6d970aea8b9713bcdf92370e815594e88f9ff39294bdf3dbc5c4190944e45ce648acd0265efcf51664  super-ttc-iosevka-etoile-11.2.0.zip
"
