# Contributor: Nick Black <dankamongmen@gmail.com>
# Maintainer: Nick Black <dankamongmen@gmail.com>
pkgname=notcurses
pkgver=3.0.0
pkgrel=0
pkgdesc="blingful character graphics and TUI library"
url="https://nick-black.com/dankwiki/index.php/Notcurses"
arch="all"
license="Apache-2.0"
# FIXME gpm-libs is still in testing. once it moves to community, dep on it,
# and add -DUSE_GPM=on to build().
makedepends="cmake doctest-dev ffmpeg-dev libdeflate-dev libunistring-dev linux-headers ncurses-dev ncurses-terminfo"
subpackages="$pkgname-dbg $pkgname-static $pkgname-dev $pkgname-doc
	$pkgname-libs $pkgname-demo ncneofetch ncls $pkgname-view $pkgname-tetris"
source="https://github.com/dankamongmen/notcurses/archive/v$pkgver/notcurses-$pkgver.tar.gz
	https://github.com/dankamongmen/notcurses/releases/download/v$pkgver/notcurses-doc-$pkgver.tar.gz
	10-putc-precopy.patch
	"

[ "$CARCH" = "riscv64" ] && options="textrels"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=None \
		-DUSE_PANDOC=off \
		-DUSE_QRCODEGEN=off \
		$CMAKE_CROSSOPTS
	make -C build
}

check() {
	env TERM=vt100 CTEST_OUTPUT_ON_FAILURE=1 make -C build test
}

package() {
	make -C build DESTDIR="$pkgdir" install
	for i in 1 3 ; do
		find "$srcdir" -maxdepth 1 -type f -iname \*.$i -exec echo "$pkgdir"/usr/share/man/man$i {} \;
		find "$srcdir" -maxdepth 1 -type f -iname \*.$i -exec install -Dm644 -t "$pkgdir"/usr/share/man/man$i {} \;
	done
}

libs() {
	amove usr/lib/libnotcurses*.so.*
}

demo() {
	amove usr/bin/notcurses-demo
	amove usr/bin/notcurses-tester
	amove usr/bin/notcurses-info
	amove usr/share/notcurses
}

ncneofetch() {
	amove usr/bin/ncneofetch
}

ncls() {
	amove usr/bin/ncls
}

view() {
	amove usr/bin/ncplayer
}

tetris() {
	amove usr/bin/nctetris
}

sha512sums="
0bc2a5256feb73a047b7d9c9fb69809aeee41c21a564f7c4be1cd3c0ab6fe68b3db2c5959f678748a2340203e8822459c370b674a3ef6867c17b4b63956f5d25  notcurses-3.0.0.tar.gz
ab4ddac027963c5c9203f48a4a51bdcecaca28cef3158c7cc505f48eb89eee9b7f3399f0fa1919ee8959b69139a051c860c72b5feef9e417750fb3141f521ab6  notcurses-doc-3.0.0.tar.gz
ab0c8533eac5feb6b167dcb4c6b814474391f8fd9a1709c445bb113cc41e76d7622ba12a2b72988e1f1414181fa27245e2cfe2ca61d4ba0fd5e9beab2c18fe59  10-putc-precopy.patch
"
