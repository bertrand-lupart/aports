# Contributor: Rob Blanckaert <basicer@gmail.com>
# Maintainer: Rob Blanckaert <basicer@gmail.com>
pkgname=luau
pkgver=0.506
pkgrel=0
pkgdesc="A fast, small, safe, gradually typed embeddable scripting language derived from Lua"
url="https://github.com/roblox/luau"
arch="all !s390x"
license="MIT"
makedepends="cmake"
source="$pkgname-$pkgver.tar.gz::https://github.com/Roblox/luau/archive/refs/tags/$pkgver.tar.gz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		$CMAKE_CROSSOPTS .
	make
}

check() {
	$builddir/Luau.UnitTest && $builddir/Luau.Conformance
}

package() {
	install -Dm755 luau "$pkgdir"/usr/bin/luau
	install -Dm755 luau-analyze "$pkgdir"/usr/bin/luau-analyze
}

sha512sums="2052b54da1e6cc2bb831859d4198f6beed5667696a7894e96c8c5382921044a5e3ca266442253fd43f0638307d0c677062437a34235b3ab887f9df65a3a9067f  luau-0.506.tar.gz"
